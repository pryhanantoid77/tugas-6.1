import axios from 'axios';
import {BASE_URL, TOKEN} from './url';

const API = async (
  url,
  options = {
    method: 'GET',
    body: {},
    head: {},
  },
) => {
  // console.log('URL1234', url);
  const request = {
    baseURL: BASE_URL,
    method: options.method,
    timeout: 10000,
    url,
    headers: options.head,
    responseType: 'json',
  };
  if (
    request.method === 'POST' ||
    request.method === 'PUT' ||
    request.method === 'DELETE'
  ) {
    request.data = options.body;
  }
  console.log('req 1234', request);

  const res = await axios(request);
  console.log('1111111', res);

  if (res.status === 200) {
    console.log('dad', res.data);
    return res.data;
  } else {
    return res;
  }
};

export default {
  getData: async () => {
    console.log('1234uytuyt');
    return API(`mobil`, {
      method: 'GET',
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('123', response);
        return response;
      })
      .catch(error => {
        console.log('error', error);
        return error;
      });
  },

  postData: async bodyy => {
    return API(`mobil`, {
      method: 'POST',
      body: bodyy,
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('tambah', response);
        alert('Data Ditambah berhasil');
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  editData: async bodyy => {
    return API(`mobil`, {
      method: 'PUT',
      body: bodyy,
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('edit', response);
        // alert('Edit berhasil');
        return response;
      })
      .catch(error => {
        return error;
      });
  },

  deleteData: async bodyy => {
    return API(`mobil`, {
      method: 'DELETE',
      body: bodyy,
      head: {
        'Content-Type': 'application/json',
        Authorization: TOKEN,
      },
    })
      .then(response => {
        console.log('delete', response);
        return response;
      })
      .catch(error => {
        return error;
      });
  },
};
