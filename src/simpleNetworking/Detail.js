import {
  ActivityIndicator,
  Dimensions,
  Image,
  Modal,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {BASE_URL, TOKEN} from '../utils/service/url';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/dist/Ionicons';
import apiProvider from '../utils/service/apiProvider';
import {convertCurrency} from '../Helper';
import Carousel from 'react-native-snap-carousel';

const Detail = ({navigation, route}) => {
  const [namaMobil, setNamaMobil] = useState('');
  const [totalKM, setTotalKM] = useState('');
  const [hargaMobil, setHargaMobil] = useState('');
  const [imageMobil, setImageMobil] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [dataMobilCarousel, setDataMobilCarousel] = useState([]);
  const SLIDER_WIDTH = Dimensions.get('window').width;
  const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.5);
  const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 4);
  const [dataImage, setDataImage] = useState('');

  var dataMobil = route.params;

  useEffect(() => {
    console.log('log123', dataMobil);
    if (route.params) {
      const data = route.params;
      setNamaMobil(data.title);
      setTotalKM(data.totalKM);
      setHargaMobil(data.harga);
      typeof setImageMobil(data?.unitImage) === 'string'
        ? setImageMobil(data.unitImage)
        : '';
    }
  }, []);

  useEffect(() => {
    getDataMobilCarousel();
  }, []);

  const getDataMobilCarousel = async () => {
    const response = await apiProvider.getData();
    console.log('get data in detail :', response);
    if (response?.items?.length) {
      setDataMobilCarousel(response.items);
    }
  };

  const editData = async () => {
    setIsLoading(true);
    const body = [
      {
        _uuid: dataMobil._uuid,
        title: namaMobil,
        harga: hargaMobil,
        totalKM: totalKM,
        unitImage:
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrhVioZcYZix5OUz8iGpzfkBJDzc7qPURKJQ&usqp=CAU',
      },
    ];

    const response = await apiProvider.editData(body);
    console.log('edittt', response);
    alert('Edit Berhasil');

    setIsLoading(false);
    setShowModal(!showModal);
    getDataMobilCarousel();
  };

  const deleteData = async () => {
    setIsLoading(true);

    const body = [
      {
        _uuid: dataMobil._uuid,
      },
    ];
    const response = await apiProvider.deleteData(body);
    console.log('hapusss', response);
    navigation.navigate('Home');

    setIsLoading(false);
    alert('Hapus data berhasil');
  };

  return (
    <View style={{flex: 1}}>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={isLoading}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: 300,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
      <Modal
        // animationType={'slide'}
        transparent={true}
        visible={showModal}
        onRequestClose={() => {
          console.log('Modal has been closed.');
        }}>
        {/*All views of Modal*/}
        {/*Animation can be slide, slide, none*/}
        <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.2)',
          }}>
          <View
            style={{
              width: '80%',
              height: '60%',
              backgroundColor: 'white',
              // justifyContent: 'center',
              // alignItems: 'center',
              borderRadius: 10,
              marginTop: 80,
              marginHorizontal: 40,
            }}>
            <ScrollView>
              <View
                style={{
                  // width: '100%',
                  padding: 15,
                }}>
                <View>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Nama Mobil
                  </Text>
                  <TextInput
                    value={namaMobil}
                    onChangeText={text => setNamaMobil(text)}
                    placeholder="Masukkan Nama Mobil"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Total Kilometer
                  </Text>
                  <TextInput
                    value={totalKM}
                    onChangeText={text => setTotalKM(text)}
                    placeholder="contoh: 100 KM"
                    style={styles.txtInput}
                  />
                </View>
                <View style={{marginTop: 20}}>
                  <Text
                    style={{fontSize: 16, color: '#000', fontWeight: '600'}}>
                    Harga Mobil
                  </Text>
                  <TextInput
                    value={hargaMobil}
                    onChangeText={text => setHargaMobil(text)}
                    placeholder="Masukkan Harga Mobil"
                    style={styles.txtInput}
                    keyboardType="number-pad"
                  />
                </View>
                <TouchableOpacity
                  onPress={() => editData()}
                  //   onPress={() => <Loading />}
                  style={styles.btnAdd}>
                  <Text style={{color: '#fff', fontWeight: '600'}}>
                    Edit Data
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={styles.btnAdd}
                  onPress={() => setShowModal(!showModal)}>
                  <Text style={{color: 'white'}}> Go Back</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
        </View>
      </Modal>
      {/* <ScrollView contentContainerStyle={{paddingBottom: 20}}> */}
      <Image
        source={{uri: dataImage ? dataImage : imageMobil}}
        style={{height: '40%', width: '100%'}}
      />
      <View
        style={{
          // flexDirection: 'row',
          marginTop: -250,
          // justifyContent: 'space-between',
          marginHorizontal: 20,
          height: 250,
          // backgroundColor: 'aqua',
          // width: '100%',
        }}>
        <Icon
          name="arrow-back"
          size={25}
          color="white"
          onPress={() => navigation.goBack('')}
        />
      </View>
      <View
        style={{
          backgroundColor: 'white',
          marginTop: -20,
          height: '100%',
          borderRadius: 20,
          padding: 20,
        }}>
        <Text>Nama Mobil : {namaMobil}</Text>
        <Text>Total Kilometer : {totalKM}</Text>
        <Text>Harga Mobil : {convertCurrency(hargaMobil, 'Rp. ')}</Text>
        <TouchableOpacity
          onPress={() => setShowModal(!showModal)}
          //   onPress={() => <Loading />}
          style={{
            marginTop: 20,
            width: '100%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: '#689f38',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Edit Data</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => deleteData()}
          style={{
            marginTop: 20,
            width: '100%',
            paddingVertical: 10,
            borderRadius: 6,
            backgroundColor: 'red',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{color: '#fff', fontWeight: '600'}}>Hapus Data</Text>
        </TouchableOpacity>
        {/* <View> */}
        <Carousel
          style
          layout={'default'}
          data={dataMobilCarousel}
          // ref={c => (this.carousel = c)}
          renderItem={({item}) => (
            <TouchableOpacity
              // onPress={() => navigation.navigate('Detail', item)}
              onPress={() => {
                setDataImage(item.unitImage),
                  setHargaMobil(item.harga),
                  setNamaMobil(item.title),
                  setTotalKM(item.totalKM);
              }}>
              <View
                style={{
                  marginTop: 15,
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  // backgroundColor: 'aqua',
                  borderColor: 'red',
                  borderWidth: 1,
                  borderRadius: 10,
                  // width: '100%',
                }}>
                <Image
                  source={{
                    uri:
                      typeof item?.unitImage === 'string' ? item.unitImage : '',
                  }}
                  style={{width: '60%', height: 100, resizeMode: 'contain'}}
                />
                <View style={{width: '80%', paddingHorizontal: 10}}>
                  <View style={{flexDirection: 'row'}}>
                    <Text>Nama: </Text>
                    <Text>{item.title}</Text>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <Text>Harga: </Text>
                    <Text style={{width: '80%'}}>
                      {convertCurrency(item.harga, 'Rp. ')}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          )}
          sliderWidth={SLIDER_WIDTH}
          itemWidth={ITEM_WIDTH}
          // containerCustomStyle={styles.carouselContainer}
          inactiveSlideShift={0}
          // onSnapToItem={index => this.setState({index})}
          // scrollInterpolator={scrollInterpolator}
          // slideInterpolatedStyle={animatedStyles}
          useScrollView={true}
        />
        {/* </View> */}
      </View>
      {/* </ScrollView> */}
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  btnAdd: {
    marginTop: 20,
    width: '100%',
    paddingVertical: 10,
    borderRadius: 6,
    backgroundColor: '#689f38',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
