import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {NavigationContainer} from '@react-navigation/native';
import Home from './Home';
// import AddData from './AddData';
import Detail from './Detail';

const Stack = createNativeStackNavigator();

const Routing = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Detail" component={Detail} />
        {/* <Stack.Screen name="AddData" component={AddData} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routing;
